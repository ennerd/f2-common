<?php
namespace F2\Container;

use F2\Common\LogicException;

class NotSupportedException extends LogicException {
    public $httpStatusCode = 501;
    public $httpStatusMessage = "Feature not supported";
}
