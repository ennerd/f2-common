<?php
namespace F2\Container;

use F2\Common\LogicException;

class NotFoundException extends LogicException {
    public $httpStatusCode = 501;
    public $httpStatusMessage = "Internal service not found";
}
