<?php
declare(strict_types=1);

namespace F2\Container\Contracts;

use Psr\Container\ContainerInterface;

interface RootContainerInterface extends ContainerInterface {
    /**
     * Factory method for the root container.
     *
     * @return RootContainerInterface
     */
    public static function getInstance(): RootContainerInterface;


    /**
     * Will automatically provide arguments from $args and the container
     * and return a closure that requires no arguments.
     *
     * @param $wireable A class name or a closure
     *
     * @return Closure A closure that accepts no arguments.
     */
    public function autowire($wireable, array $vars=[]): \Closure;

    /**
     * Convenience method for creating a service from a service specification array. A
     * service specification array has the following form:
     *
     * ```php
     * [ $type, $typeParameter, $identifier, $shared ]
     * ```
     *
     * $type must be one of 'callable', 'factory', 'file', 'class', 'instance'
     *
     * Examples:
     * [ 'callable', [ SomeClass::class, 'some_method' ], MyInterface::class, true ]
     *     - Register a callback as a factory
     *
     * [ 'factory', \F2\Container\AbstractFactory $factory ]
     *     - Register a service factory instance
     *
     * [ 'file', 'path/to/file.php', MyInterface::class, true ]
     *     - Will include the file and use whatever value is returned by that file
     *
     * [ 'class', SomeClass::class, MyInterface::class, true ]
     *     - Will instantiate the class for you
     *
     * [ 'instance', $instance ]
     *     - This instance is always shared
     *
     * $typeParameter must be:
     *     - an array or a string containing a callable (NOT a closure)
     *     - a configuration key,
     *     - a factory class name,
     *     - a file path (relative paths are resolved from APP_ROOT)
     *
     * $identifier is the interface name or any other string that serves as an identifier
     *
     * $shared is a boolean: true means that the instance is cached and reused
     *
     * @param array $spec The service specification (described above)
     */
    public function addFromSpecification( array $spec ): void;
}
