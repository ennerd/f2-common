<?php
declare(strict_types=1);

namespace F2\Container;

use League\Container\Container as LeagueContainer;
use Psr\Container\ContainerInterface;

/**
 * This class is final, because there will never be a proper way to override it
 * since it forms the basis for everything in F2. We may implement a way to override the
 * instance that is returned from getInstance()
 */
final class Container extends LeagueContainer implements Contracts\RootContainerInterface {

    /**
     * {@inheritdoc}
     */
    public static function getInstance(): Contracts\RootContainerInterface {
        static $instance;
        if (!$instance) {
            $instance = new static();
            $instance->addServiceProvider(new ManifestServiceProvider());
        }
        return $instance;
    }

    /**
     * {@inheritdoc}
     */
    public function autowire($wireable, array $vars=[], array $instances=[]): \Closure {
        if (is_callable($wireable)) {
            return function() use ($wireable, $vars, $instances) {
                $args = $this->prepareArgumentsForCallable($wireable, $vars, $instances);
                return $wireable(...$args);
            };
        } elseif (class_exists($wireable)) {
            return function() use ($wireable, $vars, $instances) {
                $args = $this->prepareArgumentsForCallable([ $wireable, '__construct' ], $vars, $instances);
                return new $wireable(...$args);
            };
        } else {
            throw new AutowiringException("Unable to auto-wire '$wireable' because it is neither a callable nor a class name.");
        }
    }

    protected function prepareArgumentsForCallable(callable $callable, array $vars, array $instances): array {
        $handler = \Closure::fromCallable($callable);
        $ref = new \ReflectionFunction($handler);
        $identity = "[".$ref->getName()." file=".$ref->getFileName()." line=".$ref->getStartLine()."]";

        $args = [];

        foreach ($ref->getParameters() as $param) {
            $type = $param->hasType() ? $param->getType()->__toString() : 'mixed';

            /**
             * Handle named vars
             */
            if (isset($vars[$param->name])) {
                // The value comes from $vars
                if ($type === 'mixed') {
                    $args[] = $vars[$param->name];
                    continue;
                } elseif ($type === 'string') {
                    $args[] = ''.$vars[$param->name];
                    continue;
                } elseif ($type === 'integer') {
                    $args[] = intval($vars[$param->name]);
                    continue;
                } elseif ($type === 'double' || $type == 'float') {
                    $args[] = floatval($vars[$param->name]);
                    continue;
                } elseif ($type === 'boolean') {
                    $args[] = !!$vars[$param->name];
                    continue;
                } else {
                    throw new AutowiringException("Could not auto-wire '$identity' because the argument \$".$param->name." provided via variables expects type unsupported type '$type'");
                }
            }

            /**
             * Untyped with default values or that allows null
             */
            if (!$param->hasType()) {
                if ($param->isDefaultValueAvailable()) {
                    $args[] = $param->getDefaultValue();
                    continue;
                }
                if ($param->allowsNull()) {
                    $args[] = null;
                    continue;
                }
                throw new AutowiringException("Could not auto-wire '$identity' because the argument \$".$param->name." has no type hint and the value is not provided via variables");
            }

            /**
             * Typed provided by $instances
             */
            foreach ($instances as $instance) {
                if ($instance instanceof $type) {
                    $args[] = $instance;
                    continue 2;
                }
            }

            /**
             * Types supported by container
             */
            if ($this->has($type)) {
                $args[] = $this->get($type);
                continue;
            }

            /**
             * Unprovided type, but with default value
             */
            if ($param->isDefaultValueAvailable()) {
                $args[] = $param->getDefaultValue();
                continue;
            }

            /**
             * Allows null
             */
            if ($param->allowsNull()) {
                $args[] = null;
                continue;
            }

            /**
             * Optional arguments is the end of arguments so we stop
             */
            if ($param->isOptional()) {
                break;
            }

            throw new AutowiringException("Could not auto-wire '$identity', unable to infer value for argument \$".$param->name);
        }

        return $args;
    }

    /**
     * {@inheritdoc}
     */
    public function addFromSpecification( array $spec ): void {
throw new Exception("NOT USED");
        // Simple validation
        if (!isset($spec[0]) || !is_string($spec[0])) {
            throw new NotSupportedException("Service specification must be an array in the format [string \$type, ...\$parameters]");
        }
        switch ($spec[0]) {
            case 'callable' : // ['callable', callable $callback, string $identifier, bool $shared=false]
                if (!(
                    // string, callable, string, bool
                    (isset($spec[3]) && is_bool($spec[3]) && is_string($spec[2]) && is_callable($spec[1])) ||
                    // string, callable, string
                    (isset($spec[2]) && is_string($spec[2]) && is_callable($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'callable' must be in format ['callable' callable \$callback, string \$identifier, bool $shared=false]");
                }
                $this->addCallableFactory( $spec[1], $spec[2], !!($spec[3] ?? false) );
                break;
            case 'factory' :
                if (!isset($spec[1]) || !($spec[1] instanceof Contracts\FactoryInterface)) {
                    throw new NotSupportedException("Service specification for 'factory' must be in format ['factory', ".Contracts\FactoryInterface::class." \$factory]");
                }
                $this->addFactory( $spec[1] );
                break;
            case 'file' : // ['file', string $path, string $identifier, bool $shared=false]
                if (!(
                    // string, string, string, bool
                    (isset($spec[3]) && is_bool($spec[3]) && is_string($spec[2]) && is_string($spec[1])) ||
                    (isset($spec[2]) && is_string($spec[2]) && is_string($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'file' must be in format ['file', string \$path, string \$identifier, bool \$shared=false]");
                }
                $this->addFileFactory( $spec[1], $spec[2], !!($spec[3] ?? false) );
                break;
            case 'class' : // ['class', string $className[, string $identifier], bool $shared=false]
                if (!(
                    // string, string, string, bool
                    (isset($spec[3]) && is_bool($spec[3]) && is_string($spec[2]) && is_string($spec[1])) ||
                    // string, string, bool
                    (isset($spec[2]) && is_bool($spec[2]) && is_string($spec[1])) ||
                    // string, string, string
                    (isset($spec[2]) && is_string($spec[2]) && is_string($spec[1])) ||
                    // string, string
                    (isset($spec[1]) && is_string($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'class' must be in format ['class', string \$className[, string \$identifier], bool \$shared=false]");
                }
                if (!class_exists($spec[1])) {
                    throw new NotFoundException("Service specification declares a non-existent class '".$spec[1]."'");
                }
                $className = $spec[1];
                $identifier = is_string($spec[2]) ? $spec[2] : $spec[1];
                $shared = isset($spec[3]) ? $spec[3] : (is_bool($spec[2]) ? $spec[2] : false);
                $this->addClassFactory( $className, $identifier, $shared );
                break;
            case 'instance' : // ['instance', object $instance[, string $identifier]]
                if (!(
                    // string, object, string
                    (isset($spec[2]) && is_string($spec[2]) && is_object($spec[1])) ||
                    // string, object
                    (isset($spec[1]) && is_object($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'instance' must be in format ['instance', object \$instance[, string \$identifier]]");
                }
                $identifier = isset($spec[2]) ? $spec[2] : get_class($spec[1]);
                $this->addInstanceFactory( $spec[1], $identifier );
                break;
            default :
                throw new NotSupportedException("Service specification type '".$spec[0]."' is unknown.");
        }
    }

    public static function cliHelpContainer() {
        echo
"F2::container(): F2\Common\Contracts\RootContainerInterface

A PSR-11 compliant container powered by league/container. Register your services with the
service container and retrieve them when they are needed. The service container powers
many of the convenience methods found in the F2 global class, which means you can
override the service provider with other implementations.

You may refer to https://container.thephpleague.com/3.x/ for documentation of the core 
APIs related to adding new services. F2 provides wrappers for the container, which enables
composer packages to register services directly without any additional configuration in
the root project.

Run 'f2 help container di' to read more about dependency injection vs the service
locator pattern.

Examples of getting access to services:

    \$logger = F2::container()->get(\Psr\Log\LoggerInterface::class);
    \$db = F2::container()->get(\PDO::clasS);
    \$dbal = F2::container()->get(\Doctrine\DBAL\Connection::class);
    \$httpClient = F2::container()->get(\GuzzleHttp\ClientInterface::class);
    \$cache = F2::container()->get(\Psr\SimpleCache\CacheInterface::class);

In F2, many services will be even simpler to access via the F2-methods:

    \$logger = F2::log();
    \$db = F2::db();
    \$dbal = F2::dbal();
    \$httpClient = F2::httpClient();


Adding services via \$CONFIG_ROOT/container/services.php:

<?php return [
    [ 'factory', new class extends \F2\Container\AbstractFactort {} ],
    [ 'class', Some\ClassName::class, Some\InterfaceName::class, true ],
    [ 'callable', [ Some\ClassName::class, 'getInstance' ], Some\InterfaceName::class, true ],
    [ 'file', 'path/to/file.php', Some\InterfaceName::class, true ],
    [ 'instance', new Some\ClassName() ],
];
?>


Adding a services via the Manifest class:

<?php
namespace Some\Namespace;

class Manifest extends \F2\Common\AbstractManifest {
    public function registerConfigDefaults(array \$config) {
        \$config['container/services'][] = [ 'class', Some\ClassName::class, Some\InterfaceName::class, true ];
        return \$config;
    }
}
?>



";
    }

    public static function cliHelpContainerDi() {
        echo

"Dependency Injection is so cool! Or is it?

Example: Dependency Injection

<?php
class MyController {
    protected \$logger;
    public function __construct(Psr\Log\LoggerInterface \$logger) {
        \$this->logger = \$logger;
    }

    public function hey() {
        return 'Ho!';
    }

    public function ho() {
        \$this->logger->warning(\"He said 'ho'!\");
        return 'Derp!';
    }
}
?>

Dependency Injection is a pattern that evolved from programming languages such as java,
where the application is compiled, then bootstraps - and finally serves many requests.

In PHP, the application is compiled, then bootstrapped, and finally in serves only ONE
request. While the compilation is often cached by the opcache, the entire application
is boostrapped for every request.

Dependency injection means that services that might never be used, will be injected
into your class - even if the service is not going to be used.


Example: Service Locator

<?php
class MyController {
    public function hey() {
        return 'Ho!';
    }

    public function ho() {
        F2::container()->get(Psr\Log\LoggerInterface::class)->warning(\"He said 'ho'!\");
        return 'Derp!';
    }
}
?>

or even

<?php
class MyController {
    protected \$logger;
    public function __construct() {
        \$this->logger = F2::container()->get(Psr\Log\LoggerInterface::class);
    }

    public function hey() {
        return 'Ho!';
    }

    public function ho() {
        \$this->logger->warning(\"He said 'ho'!\");
        return 'Derp!';
    }
}
?>

The PSR-11 container can also be used as a 'service locator'. Your code will request
the service WHENEVER it is needed. This is different from having all the services 
instantiated and attached to your controller class on every request.

The autowiring that goes on in frameworks such as Laravel and Symfony will provide
you with a full database connection, even if you discover that the entire request
can be satisfied from the Redis cache.

There are ways to mitigate this overhead, for example by creating lazy versions of
the services. But why bother?

";
    }
}
