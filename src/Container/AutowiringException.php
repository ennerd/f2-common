<?php
declare(strict_types=1);

namespace F2\Container;

class AutowiringException extends Exception {}
