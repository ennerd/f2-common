<?php
declare(strict_types=1);
namespace F2\Container;

use F2;

class ManifestServiceProvider extends AbstractServiceProvider {

    public function boot() {
        /**
         * Resolve namespaces
         */
        $config = F2::config('container/services', []);

        if (isset($config[''])) {
            $first = current($config['']);
            throw new Exception("container/services config should not define namespaces anymore (['container/services'][''][] = ".json_encode($first)." is not allowed)");
        }

        foreach ($config as $serviceDefinition) {
            $this->parseServiceDefinition($serviceDefinition);
        }
    }

    public function provides(string $service): bool {
        return false;
    }

    public function register() {

    }

    /**
     * Convenience method for creating a service from a service specification array. A
     * service specification array has the following form:
     *
     * ```php
     * [ $type, $typeParameter, $identifier, $shared ]
     * ```
     *
     * $type must be one of 'callable', 'factory', 'file', 'class', 'instance'
     *
     * Examples:
     * [ 'callable', [ SomeClass::class, 'some_method' ], MyInterface::class, true ]
     *     - Register a callback as a factory
     *     - 'arguments' => array is an optional array of string identifiers or League\Container\Argument\*
     *
     * [ 'factory', \F2\Container\AbstractFactory $factory ]
     *     - Register a service factory instance
     *
     * [ 'file', 'path/to/file.php', MyInterface::class, true ]
     *     - Will include the file and use whatever value is returned by that file
     *
     * [ 'class', SomeClass::class, MyInterface::class, true ]
     *     - Will instantiate the class for you
     *     - 'arguments' => array is an optional array of string identifiers or League\Container\Argument\*
     *
     * [ 'instance', $instance ]
     *     - This instance is always shared
     *
     * $typeParameter must be:
     *     - an array or a string containing a callable (NOT a closure)
     *     - a configuration key,
     *     - a factory class name,
     *     - a file path (relative paths are resolved from APP_ROOT)
     *
     * $identifier is the interface name or any other string that serves as an identifier
     *
     * $shared is a boolean: true means that the instance is cached and reused
     *
     * @param array $spec The service specification (described above)
     */
    protected function parseServiceDefinition( array $spec ): void {
        // Simple validation
        if (!isset($spec[0]) || !is_string($spec[0])) {
            throw new NotSupportedException("Service specification must be an array in the format [string \$type, ...\$parameters]");
        }

        $definition = null;

        switch ($spec[0]) {
            case 'callable' : // ['callable', callable $callback, string $identifier, bool $shared=false]
                if (!(
                    // string, callable, string, bool
                    (isset($spec[3]) && is_bool($spec[3]) && is_string($spec[2]) && is_callable($spec[1])) ||
                    // string, callable, string
                    (isset($spec[2]) && is_string($spec[2]) && is_callable($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'callable' must be in format ['callable' callable \$callback, string \$identifier, bool $shared=false]");
                }
                $callback = $spec[1];
                $identifier = $spec[2];
                $shared = $spec[3] ?? false;
                $definition = $this->getContainer()->add( $identifier, $callback )
                    ->setShared( $shared );
                break;
            case 'file' : // ['file', string $path, string $identifier, bool $shared=false]
                if (!(
                    // string, string, string, bool
                    (isset($spec[3]) && is_bool($spec[3]) && is_string($spec[2]) && is_string($spec[1])) ||
                    (isset($spec[2]) && is_string($spec[2]) && is_string($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'file' must be in format ['file', string \$path, string \$identifier, bool \$shared=false]");
                }
                $this->addFileFactory( $spec[1], $spec[2], !!($spec[3] ?? false) );
                break;
            case 'class' : // ['class', string $className[, string $identifier], bool $shared=false]
                if (!(
                    // string, string, string, bool
                    (isset($spec[3]) && is_bool($spec[3]) && is_string($spec[2]) && is_string($spec[1])) ||
                    // string, string, bool
                    (isset($spec[2]) && is_bool($spec[2]) && is_string($spec[1])) ||
                    // string, string, string
                    (isset($spec[2]) && is_string($spec[2]) && is_string($spec[1])) ||
                    // string, string
                    (isset($spec[1]) && is_string($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'class' must be in format ['class', string \$className[, string \$identifier], bool \$shared=false]");
                }
                if (!class_exists($spec[1])) {
                    throw new NotFoundException("Service specification declares a non-existent class '".$spec[1]."'");
                }
                $className = $spec[1];
                $identifier = is_string($spec[2]) ? $spec[2] : $spec[1];
                $shared = isset($spec[3]) ? $spec[3] : (is_bool($spec[2]) ? $spec[2] : false);

                $definition = $this->getContainer()->add( $identifier, $className )
                    ->setShared( $shared );
                break;
            case 'instance' : // ['instance', object $instance[, string $identifier]]
                if (!(
                    // string, object, string
                    (isset($spec[2]) && is_string($spec[2]) && is_object($spec[1])) ||
                    // string, object
                    (isset($spec[1]) && is_object($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'instance' must be in format ['instance', object \$instance[, string \$identifier]]");
                }
                $identifier = isset($spec[2]) ? $spec[2] : get_class($spec[1]);
                $this->addInstanceFactory( $spec[1], $identifier );
                break;
            default :
                throw new NotSupportedException("Service specification type '".$spec[0]."' is unknown.");
        }

        if (isset($spec['tags'])) {
            foreach ($spec['tags'] as $tag) {
                if (!is_string($tag)) {
                    throw new NotSupportedException("\$spec['tags'] must be an array of strings");
                }
                $definition->addTag($tag);
            }
        }

        if (isset($spec['arguments'])) {
            foreach ($spec['arguments'] as $argument) {
                $definition->addArgument($argument);
            }
        }
    }
}
