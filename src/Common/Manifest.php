<?php
declare(strict_types=1);

namespace F2\Common;

use Psr\Log\LoggerInterface;

class Manifest extends AbstractManifest {

    public function getF2Methods(): iterable {
        // use f2/container if exists
        yield "events" => Contracts\EventServiceInterface::class;
        yield "config"  => [Config::class, 'config'];
        yield "env"     => [Config::class, 'env'];
        yield "log"     => LoggerInterface::class;
    }

    public function registerConfigDefaults(array $config): array {
        // Works with f2/container if it is in use
        $config['container/services'][] = [ 'class', EventService::class, Contracts\EventServiceInterface::class, true ];
        $config['container/services'][] = [ 'class', Config::class, Contracts\ConfigInterface::class, true ];
        $config['container/services'][] = [ 'class', Psr\Log\ErrorLogLogger::class, LoggerInterface::class, true ];
        return $config;
    }

    /**
     * Returns all discovered manifests. Note that this function is not intended
     * to be used in production due to performance reasons - it is mainly used to 
     * pre-generate the F2 class.
     *
     * Another problem with this function is that it only discovers Manifests in
     * packages that use the F2 namespace. We'll try to fix this.
     */
    public static function getAllManifests(): iterable {
        $composer = Config::getComposerClassLoader();

        $manifests = [
            // Extra manifests hosted in f2-common
//            static::getManifest(\F2\Container\Manifest::class),
            ];

        foreach($composer->getPrefixesPsr4() as $namespace => $path) {
            if (substr($namespace, 0, 3)==="F2\\") {
                $className = rtrim($namespace, "\\")."\\Manifest";
                if (class_exists($className) && ($manifest = static::getManifest($className))) {
                    $manifests[] = $manifest;
                }
            }
        }

        foreach ($composer->getClassMap() as $className => $path) {
            if (substr($className, 0, 3)==='F2\\') {
                if ($manifest = static::getManifest(substr($className, 0, strrpos($className, "\\")).'\\Manifest')) {
                    $manifests[] = $manifest;
                }
            }
        }

        foreach ($composer->getPrefixes() as $namespace => $path) {
            if (substr($namespace, 0, 3)==='F2\\') {
                if ($manifest = static::getManifest($namespace.'\\Manifest')) {
                    $manifests[] = $manifest;
                }
            }
        }

        // Sort manifests according to dependency graph.
        $maxDelays = 100;
        $yielded = [];
        while (sizeof($manifests) > 0) {
            $candidate = array_shift($manifests);
            $skipCandidate = false;
            foreach ($candidate->getDependencies() as $dependency) {
                if (!isset($yielded[$dependency])) {
                    // Push the manifest to be yielded later
                    if ($maxDelays-- === 0) {
                        throw new LogicException("The manifest '".get_class($candidate)."' depends on '$dependency', which appears to be unresolveable.");
                    }
                    $manifests[] = $candidate;
                    $skipCandidate = true;
                    break;
                }
            }
            if (!$skipCandidate) {
                $yielded[get_class($candidate)] = $candidate;
                yield $candidate;
            }
        }
    }

    protected static function getManifest(string $className): ?AbstractManifest {
        if (class_exists($className) && is_subclass_of($className, AbstractManifest::class, true)) {
            return new $className();
        }
        return null;
    }
}
