<?php
namespace F2\Common\Util;

/**
 * Represents a future value. This is useful in a command pattern, where
 * the next instructions can use values from previous instructions.
 */
class FutureValue {

    const TYPE_INT = 'int';
    const TYPE_FLOAT = 'double'; // (for historical reasons "double" is returned in case of a float, and not simply "float")
    const TYPE_DOUBLE = 'double';
    const TYPE_STRING = 'string';
    const TYPE_ARRAY = 'array';
    const TYPE_OBJECT = 'object';

    /**
     * If the value is a FutureValue, check that it has a value, check the type
     * and return the value.
     */
    public static function resolve($value, ?string $type) {
        if ($value instanceof static) {
            if (!$value->hasValue()) {
                throw new \InvalidArgumentException("The argument has not received a value");
            }
            $value = $value->getValue();
        }
        if ($type !== null) {
            if ($type === 'float') {
                $type = 'double';
            }
            if (gettype($value) === $type) {
                return $value;
            }
            if ($type === 'double' && is_int($value)) {
                return $value;
            }
            if (is_object($value) && $value instanceof $type) {
                return $value;
            }
            throw new \InvalidArgumentException("The value is not a '$type', got type '".gettype($value)."' = '".serialize($value)."'.");
        }
        return $value;
    }

    public function __construct(string $type=null) {
        $this->type = $type;
    }

    protected $type, $value, $hasValue = false;

    public function hasValue(): bool {
        return $this->hasValue;
    }

    public function setValue($value) {
        if ($this->type !== null) {
            $type = gettype($value);
            if (!($type === $this->type || (is_object($value) && $value instanceof $this->type))) {
                throw new \InvalidArgumentException("Wrong type. Expects '".$this->type."'");
            }
        }
        $this->hasValue = true;
        $this->value = $value;
    }

    public function getValue() {
        if (!$this->hasValue) {
            throw new \F2\LogicException("Handle has not been given a value yet.");
        }
        return $this->value;
    }

    public function getType(): ?string {
        return $this->type;
    }
}
