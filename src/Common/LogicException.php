<?php
declare(strict_types=1);

namespace F2\Common;

use F2\Common\Contracts\ExceptionInterface;

class LogicException extends \LogicException implements ExceptionInterface {
    use ExceptionTrait;

    public $httpStatusMessage = "Internal logic error";
}
