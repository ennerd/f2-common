<?php
declare(strict_types=1);

namespace F2\Common;

trait ExceptionTrait {
    public function getHttpStatusCode(): int {
        return \property_exists($this, 'httpStatusCode') ? $this->httpStatusCode : 500;;
    }

    public function getHttpStatusMessage(): string {
        return \property_exists($this, 'httpStatusMessage') ? $this->httpStatusMessage : "Internal server error";;
    }
}
