<?php
declare(strict_types=1);

namespace F2\Common;

class PreloaderException extends Exception {
    public $httpStatusCode = 501;
    public $httpStatusMessage "Feature not implemented";
}
