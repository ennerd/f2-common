<?php
declare(strict_types=1);

namespace F2\Common\Contracts;

use Psr\Log\LoggerInterface as PsrLoggerInterface;

interface LoggerInterface extends PsrLoggerInterface {

    /**
     * Cancelable event emitted for every new log line.
     */
    const LOG_EVENT = self::class.'::LOG_EVENT';

}
