<?php
namespace F2\Common\Contracts;

use Throwable;

interface ExceptionInterface extends Throwable {
    /**
     * Return the HTTP status code to use
     */
    public function getHttpStatusCode(): int;

    /**
     * Return the HTTP status message to use
     */
    public function getHttpStatusMessage(): string;
}
