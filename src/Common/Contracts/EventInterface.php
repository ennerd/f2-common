<?php
declare(strict_types=1);
namespace F2\Common\Contracts;

interface EventInterface {

    public function getName(): string;

    public function getData(): array;

    public function isCancelled(): bool;

}
