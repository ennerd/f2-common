<?php
declare(strict_types=1);
namespace F2\Common\Contracts;

/**
 * Interface for the central event emitter used for hooking into shared functionality.
 */
interface EventServiceInterface extends EventEmitterInterface {
}
