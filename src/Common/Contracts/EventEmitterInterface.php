<?php
declare(strict_types=1);
namespace F2\Common\Contracts;

/**
 * Instances that emit events.
 *
 * CONVENTION!
 *
 * By convention, all classes that implement EventEmitterInterface
 * MUST declare constants with event names like this:
 *
 *     const MY_EVENT = self::class.'::MY_EVENT';
 *
 * so that
 *
 *     SomeClass::SOME_EVENT === 'SomeClass::SOME_EVENT'
 *
 */
interface EventEmitterInterface {

    /**
     * Listen to events of type '$name'
     */
    public function on(string $name, callable $handler): void;

    /**
     * Unlisten the event handler
     */
    public function off(string $name, callable $handler): bool;

    /**
     * Emit an event. For example F2::events()->emit(new F2\Common\Event(F2::ON_LOAD_EVENT));
     */
    public function emit(EventInterface $event): EventInterface;
}
