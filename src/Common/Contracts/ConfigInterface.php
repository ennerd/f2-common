<?php
declare(strict_types=1);

namespace F2\Common\Contracts;

interface ConfigInterface {
    public static function config(string $configName, $defaultValue = null);
    public static function env(string $name): ?string;
}
