<?php
namespace F2\Common\Contracts\Router;

/**
 * The Router Interface does not worry about how routes are registered. It should
 * be able to query 
 */
interface RouterInterface {

    /**
     * Given a path, sometimes including a query string, resolve the route
     * to a RouterResultInterface.
     */
    public function resolve(string $path): RouterResultInterface;

}
