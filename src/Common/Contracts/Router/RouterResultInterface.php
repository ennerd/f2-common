<?php
namespace F2\Common\Contracts\Router;

interface RouterResultInterface {

    public function getPath(): string;
    public function getClosure(): \Closure;
    public function getParams(): array;

}
