<?php
namespace F2\Common\Psr\SimpleCache;

use F2\Common\LogicException;
use Psr\SimpleCache\CacheInterface;

abstract class AbstractSimpleCache implements CacheInterface {
    /**
     * Retrieve a string value from the backend.
     * Returns an array [ string data, int version_id ] or null
     */
    abstract public function backendGet(string $key): ?array;

    /**
     * Store a string value to the backend. The value expires when time() > $expirationTime
     * Returns the version id which can be used in backendGet. Returns null if version check fails,
     * or if $value === null, which means delete.
     */
    abstract public function backendSet(string $key, ?string $value, int $expirationTime, ?int $versionCheck): ?int;

    /**
     * Clear all data from the backend
     */
    abstract public function backendClear(): bool;


    public function get($key, $default=null) {
        $item = $this->backendGet($key);
        if ($item === null) {
            return $default;
        }
        $item = json_decode($item[0], true);
        return $item['v'];
    }

    public function set($key, $value, $ttl=null) {
        $ttl = static::toSeconds($ttl);
        $item = ['k' => $key, 'v' => $value];
        $this->backendSet( $key, json_encode($item), $ttl, null );
        return true;
    }

    public function delete($key) {
        $item = $this->get($key);
        if ($item === null) {
            return false;
        }
        return $this->set($key, null);
    }

    public function clear() {
        return $this->backendClear();
    }

    public function getMultiple($keys, $default=null) {
        foreach ($keys as $key) {
            $result[$key] = $this->get($key, $default);
        }
        return $result;
    }

    public function setMultiple($values, $ttl=null) {
        foreach ($values as $key => $value) {
            if (!$this->set($key, $value, $ttl)) {
                return false;
            }
        }
        return true;
    }

    public function deleteMultiple($keys) {
        foreach ($keys as $key) {
            $this->delete($key);
        }
        return true;
    }

    public function has($key) {
        return $this->get($key) !== null;
    }

    protected static function toSeconds($interval): int {
        if ($interval === null) {
            return 31556926;
        } elseif (is_int($interval)) {
            return $interval;
        } elseif ($interval instanceof \DateInterval) {
            $reference = new DateTimeImmutable;
            $endTime = $reference->add($interval);
            return $endTime->getTimestamp() - $reference->getTimestamp();
        } else {
            throw new class("Illegal type for interval") extends LogicException implements InvalidArgumentException {};
        }
    }
}
