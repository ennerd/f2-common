<?php
declare(strict_types=1);

namespace F2\Common\Psr\Log;

use Psr\Log\LogLevel;
use F2;
use F2\Common\Event;
use F2\Common\Contracts\LoggerInterface;

abstract class AbstractLogger implements LoggerInterface {

    /**
     * Provides logging with filtering
     */
    final public function log($level, $message, array $context = []) {
        $event = new Event(LoggerInterface::LOG_EVENT, [ 'src' => $this, 'level' => $level, 'message' => $message, 'context' => $context ]);

        F2::events()->emit($event);

        if ($event->isCancelled()) {
            return;
        }

        $this->realLog($event->level, $event->message, $event->context);
    }

    /**
     * Pass on the log message to some logging mechanism or permanent storage.
     */
    abstract protected function realLog($level, $message, array $context = []);

    /**
     * Convenience function
     */
    public function emergency($message, array $context = []) {
        return $this->log(LogLevel::EMERGENCY, $message, $context);
    }

    /**
     * Convenience function
     */
    public function alert($message, array $context = []) {
        return $this->log(LogLevel::ALERT, $message, $context);
    }

    /**
     * Convenience function
     */
    public function critical($message, array $context = []) {
        return $this->log(LogLevel::CRITICAL, $message, $context);
    }

    /**
     * Convenience function
     */
    public function error($message, array $context = []) {
        return $this->log(LogLevel::ERROR, $message, $context);
    }

    /**
     * Convenience function
     */
    public function warning($message, array $context = []) {
        return $this->log(LogLevel::WARNING, $message, $context);
    }

    /**
     * Convenience function
     */
    public function notice($message, array $context = []) {
        return $this->log(LogLevel::NOTICE, $message, $context);
    }

    /**
     * Convenience function
     */
    public function info($message, array $context = []) {
        return $this->log(LogLevel::INFO, $message, $context);
    }

    /**
     * Convenience function
     */
    public function debug($message, array $context = []) {
        return $this->log(LogLevel::DEBUG, $message, $context);
    }

    /**
     * Provides a human readable log line by replacing {key} with the value
     * from $context.
     */
    protected function interpolate(string $message, array $context) {
        $pairs = [];
        $problem = null;
        foreach ($context as $key => $val) {
            if (is_scalar($val) || (is_object($val) && method_exists($val, '__toString'))) {
                $pairs["{".$key."}"] = str_replace("\\/", "/", json_encode($val));
            } elseif ($problem===null) {
                $pairs["{".$key."}"] = "NULL";
            } else {
                $problem = [ "Context variable '{key}' for logger is not stringable", [ 'key' => $key ]];
            }
        }
        if ($problem !== null) {
            $this->notice($problem[0], $problem[1]);
        }
        return strtr($message, $pairs);
    }
}
