<?php
declare(strict_types=1);

namespace F2\Common\Psr\Log;

abstract class StreamLogger extends AbstractLogger {

    abstract protected function getLogStream();

    protected function realLog($level, $message, array $context = []) {
        $message = ''.$message;
        fwrite($this->getLogStream(), date('c').' '.$this->interpolate($message, $context)."\n");
    }

}
