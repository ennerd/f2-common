<?php
declare(strict_types=1);

namespace F2\Common\Psr\Log;

class ErrorLogLogger extends AbstractLogger {

    protected function realLog($level, $message, array $context = []) {
        $message = ''.$message;
        if (PHP_SAPI !== 'cli') {
            \error_log(trim(addslashes('['.$level.'] '.$this->interpolate($message, $context))), 4);
        } else {
            \error_log(trim(date('c').' ['.$level.'] '.$this->interpolate($message, $context))."\n", 3, 'php://stderr');
        }
    }

}

