<?php
declare(strict_types=1);

namespace F2\Common\Psr\Log;

class StderrLogger extends StreamLogger {
    protected function getLogStream() {
        return STDERR;
    }
}
