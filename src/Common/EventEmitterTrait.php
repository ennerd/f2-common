<?php
declare(strict_types=1);
namespace F2\Common;

use F2\Common\Contracts\EventInterface;

trait EventEmitterTrait {
    protected $eventEmitterHandlers = [];

    public function on(string $eventName, callable $handler, int $priority=0): void {
        $this->eventEmitterHandlers[$eventName][$priority][] = $handler;
    }

    public function off(string $eventName, callable $handler): bool {
        if (!isset($this->eventEmitterHandlers[$eventName])) {
            return false;
        }

        foreach ($this->eventEmitterHandlers[$eventName] as $priority => $eventEmitterHandlers) {
            foreach ($eventEmitterHandlers as $index => $_handler) {
                if ($handler === $_handler) {
                    unset ($this->eventEmitterHandlers[$eventName][$priority][$index]);
                    return true;
                }
            }
        }
        return false;
    }

    public function emit(Contracts\EventInterface $event): EventInterface {
        $eventName = $event->getName();

        if (!isset($this->eventEmitterHandlers[$eventName])) {
            return $event;
        }

        $priorities = $this->eventEmitterHandlers[$eventName];
        ksort($priorities);
        foreach ($priorities as $priority => $eventEmitterHandlers) {
            foreach ($eventEmitterHandlers as $handler) {
                call_user_func($handler, $event);
            }
        }

        return $event;
    }
}
