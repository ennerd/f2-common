<?php
declare(strict_types=1);

namespace F2\Common;

use F2\Common\Contracts\{EventServiceInterface, EventInterface};

class EventService implements EventServiceInterface {
    use SingletonTrait;
    use EventEmitterTrait {
        emit as private realEmit;
    }

/*
    public function __construct() {
        \F2::events();
    }
*/
    /**
     * Emitted whenever the EventService is about to dispatch an event.
     * If this event is cancelled, the original event will not be dispatched.
     */
    const FILTER_EVENT = self::class.'::FILTER_EVENT';

    /**
     * Emitted whenever an event is cancelled via the self::FILTER_EVENT event.
     */
    const CANCELLED_EVENT = self::class.'::CANCELLED_EVENT';

    /**
     * Emitted after all events that WAS NOT cancelled via the self::FILTER_EVENT event.
     */
    const COMPLETED_EVENT = self::class.'::COMPLETED_EVENT';

    public function emit(EventInterface $event): EventInterface {
        // Emit a self::FILTER_EVENT allowing all event dispatches to be monitored
        $filterEvent = new Event(self::FILTER_EVENT, [ 'src' => self::class, 'event' => $event ]);
        $filterEvent = $this->realEmit($filterEvent);
        if (!$filterEvent->isCancelled()) {
            $event = $this->realEmit($event);
            $this->realEmit(new Event(self::COMPLETED_EVENT, [ 'src' => self::class, 'event' => $event ]));
        } else {
            $this->realEmit(new Event(self::CANCELLED_EVENT, [ 'src' => self::class, 'event' => $event ]));
        }
        return $event;
    }

    public static function cliHelpEvents() {
        echo
"F2::events()

The event service is a central event dispatcher for your application. Use this
to integrate with the framework, and provide additional integration points
in your library code.

Note that you can implement the EventInterface in your own classes - you don't
have to use the central event dispatcher, and in some cases you shouldn't use
the central event dispatcher.

Emitting an event:

<?php
use F2\Common\Event;

class SomeController {
    // The event MUST be declared as a constant which references itself.
    const SOME_EVENT = self::class.'::SOME_EVENT';
    const ANOTHER_EVENT = 'SomeController::ANOTHER_EVENT';

    public function __construct() {
        // Listen for an event
        F2::events()->on(static::SOME_EVENT, [ \$this, 'on_some_event' ]);
    }

    public function __destruct() {
        // Stop listening for the event
        F2::events()->off(static::SOME_EVENT, [ \$this, 'on_some_event' ]);
    }

    public function someFunction() {
        // Emit an event
        F2::events()->emit(new Event(static::SOME_EVENT, ['src' => \$this]));
    }

    // The event handler
    public function on_some_event(Event \$e) {
        F2::log()->info('Received event notification');
    }
}
?>
";
    }

}
