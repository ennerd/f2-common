<?php
declare(strict_types=1);
namespace F2\Common;

trait SingletonTrait {
    public static function getInstance() {
        static $instance;
        if ($instance === null) {
            $instance = new static();
        }
        return $instance;
    }
}
