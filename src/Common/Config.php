<?php
declare(strict_types=1);

namespace F2\Common;

use F2;
use function getenv, file_exists, is_callable, call_user_func, is_dir, is_file, dirname, file_get_contents, function_exists, json_decode;


/**
 * Access to library configuration
 */
class Config implements Contracts\ConfigInterface {
    public static function cliHelpConfig() {
        echo 
"F2::config(string \$configPath, \$defaultValue = null): mixed

    \$configPath example: 'cache/redis server'

Please note that developers are encouraged to use config
only for fixed configuration details that will be committed
to a source code repository. Your end users should normally
only have to defined environment variables which you can
use via the F2::env(string \$name) function.

When calling F2::config('cache/redis server', null) we find
the configuration value like this:

    1. Is \$GLOBALS['f2']['config']['cache/redis'] declared?
       - If it is declared, step 2, 3 and 4 will never happen
       - Check if it has an array key or an object property named
         'server' and return it.

       Values here are supposed to be provided by other composer
       packages.

    2. Is it set in getenv('cache/redis')?
       - If the env variable exists, step 3 and 4 will never happen.
       - Check if it is json encoded, and look for the key 'server'.

       In most cases, you would not provide this type of configuration
       data through environment variables.

    3. If F2::env('CONFIG_ROOT') declared, and does the directory exist?
       - If a file exists named \$CONFIG_ROOT/cache/redis.php, then step 4
         will never happen.
       - Load the file and check if it returns an array or an object that
         has the key 'server'.

       In these PHP files you can easily program logic that discovers the
       configuration values from the environment, or you can simply return
       an array:

       <?php return [ 'server' => '127.0.0.1', 'port' => 3001 ];

    4. Is a default value provided in
       \$GLOBALS['f2']['config']['defaults']['cache/redis']?
       - If it is, check for the key 'server' and return it - or return
         null if it does not exist.

    5. Return \$defaultValue as provided when calling F2::config()

";
    }

    public static function cliHelpEnv() {
        echo
"F2::env(string \$name): ?string

    \$name example: 'DATABASE_URL'

Environment variables are retrieved in this order:

    1. Is it already set in the environment?
    2. Is it defined in ROOT/.env?
    3. Is it defined in ROOT/default.env

If the value cannot be found in any of the above locations, the
following is a description of each value and their defaults:

ROOT            The topmost folder for your entire project. Defaults to the folder
				where composer.json is located.

DOCUMENT_ROOT   Normally provided by the web server. Throws exception in cli mode.
                Is normally \$ROOT/public or \$ROOT/html

TEMP_ROOT       Temporary work space. Files stored here can disappear and reapper
                any time. Defaults to a unique folder in /tmp - provided by
                sys_get_temp_dir().

WORK_ROOT       Persistent shared storage where files will not be deleted between
                requests. If using NFS, mount using synchronous writes. Normally
                you would not use this path in a multiple web server setup.
                Defaults to \$ROOT/var.

SHARED_ROOT     Shared permanent storage. Intended to be powered by sshfs, goofys,
                s3fs or NFS. Don't use file append or random access writes here.
                Defaults to \$ROOT/shared

USERFILES_ROOT  The path to a folder that is accessible via the web server. The
                path must be shared between web servers using NFS or similar
                technology. The path must correspond to the base URL provided in
                USERFILES_URL. Defaults to \$DOCUMENT_ROOT/files

APP_ROOT        The place where your application files are stored. Defaults to
                \$ROOT/app

CONFIG_ROOT     The place where configuration files are found. Defaults to
                \$APP_ROOT/config

VIEWS_ROOT      The place where template files are stored for your application.
                Defaults to \$APP_ROOT/views

SEEDS_ROOT      The place where database seed files are stored. Defaults to
                \$APP_ROOT/seeds

MIGRATIONS_ROOT Database migration files. Defaults to \$APP_ROOT/migrations

USERFILES_URL   The web path to files stored in USERFILES_ROOT. The value is
                inferred from \$BASE_URL only if USERFILES_ROOT is inside
                DOCUMENT_ROOT.

DATABASE_URL    A database URL in the form defined by Doctrine DBAL at
                https://symfony.com/doc/current/doctrine/dbal.html. Defaults to
                sqlite:///\$WORK_ROOT/development-database.sqlite

";

    }

    /**
     * Find the configuration data from environment or from configuration files
     *
     * Sources for configuration data are checked in this order:
     *
     * 1. $GLOBALS['f2']['config'][$configName]
     * 2. Environment variables, or the .env file located at the project root.
     * 3. If the CONFIG_ROOT environment variable is set, we will look for a file named CONFIG_ROOT/$configName.php
     * 4. Finally, we'll look in $GLOBALS['f2']['config']['defaults'][$configName]
     */
    public static function config(string $configPath, $defaultValue = null) {
        // We can assume this is already loaded since bootstrap.php must have been loaded.
        $config = &$GLOBALS["f2"]["config"];

        $configPath = explode(" ", $configPath);
        $configName = array_shift($configPath);
        if (sizeof($configPath) > 0) {
            $root = static::config($configName, $defaultValue);
            while (sizeof($configPath) > 0) {
                $next = array_shift($configPath);
                if (is_numeric($next) && intval($next) == $next) {
                    $next = intval($next);
                }
                if (is_array($root) && isset($root[$next])) {
                    $root = $root[$next];
                } elseif (is_object($root) && isset($root->$next)) {
                    $root = $root->$next;
                } else {
                    if (function_exists('json_decode')) {
                        $root = @json_decode($root, true);
                        if (is_array($root) && isset($root[$next])) {
                            $root = $root[$next];
                        } elseif (is_object($root) && isset($root->next)) {
                            $root = $root->$next;
                        } else {
                            return null;
                        }
                    } else {
                        return null;
                    }
                }
            }
            return $root;
        }

        // 1 (check $GLOBALS['f2']['config'][$here], it also acts like a cache of previously resolved configs)
        if (isset($config[$configName])) {
            if (is_callable($config[$configName])) {
                return $config[$configName] = call_user_func($config[$configName]);
            } else {
                return $config[$configName];
            }
        }

        // 2 Environment variables
        if ($result = static::env($configName)) {
            return $config[$configName] = $result;
        }

        // 3. Check for a file in CONFIG_ROOT
        try {
            $configRoot = $config["CONFIG_ROOT"] ?? static::env("CONFIG_ROOT") ?? null;
            if ($configRoot !== null) {
                $path = $configRoot . "/" . $configName . ".php";
                if (file_exists($path)) {
                    return $config[$configName] = include($path);
                }
            }
        } catch (\Throwable $e) {
            // Ignoring errors here, because we'll simply consider it as if the config was not found.
        }

        // Import any default config
        if (!isset($config["defaults"][$configName])) {
            $defaults = F2::getConfigDefaults();
            if (isset($defaults[$configName])) {
                $config["defaults"][$configName] = $defaults[$configName];
            }
        }

        // 4. Check default config
        if (isset($config["defaults"][$configName])) {
            if (!is_callable($config["defaults"][$configName])) {
                return $config[$configName] = $config['defaults'][$configName];
            } else {
                return $config[$configName] = call_user_func($config["defaults"][$configName]);
            }
        }

        if ($defaultValue !== null) {
            return $defaultValue;
        }

        throw new MissingConfigException($configName);
    }

    public static function env(string $name): ?string {
        static $didFirstRun = false, $cached = [];

        /**
         * Check early (may not be using composer)
         */
        if (isset($cached[$name])) {
            return (string) $cached[$name];
        }
        if (false !== ($value = getenv($name) ?? false)) {;
            return (string) $cached[$name] = $value;
        }
        if ($name === 'ROOT') {
            return (string) $GLOBALS['f2']['config']['ROOT'] = $GLOBALS['f2']['config']['ROOT'] ?? $GLOBALS['f2']['config']['defaults']['ROOT'];
        }

        /**
         * Find the composer root path and do some early maintenance
         */
        if (!$didFirstRun) {
            $root = static::env('ROOT');
            if (!$root) {
                throw new ConfigException("Could not find the project ROOT. Set the ROOT path as an environment variable.");
            }

            /**
             * Load .env file if it exists
             */
            if (file_exists($path = $root.'/.env')) {
                foreach (\M1\Env\Parser::parse(file_get_contents($path)) as $key => $val) {
                    $cached[$key] = $val;
                }
            }

            /**
             * Load default.env file if it exists
             */
            if (file_exists($path = $root.'/default.env')) {
                foreach (\M1\Env\Parser::parse(file_get_contents($path)) as $key => $val) {
                    if (!isset($cached[$key])) {
                        $cached[$key] = $val;
                    }
                }
            }

            $didFirstRun = true;
            return (string) static::env($name);
        }

        /**
         * Resolve env variable
         */
        $resolvers = [
            /**
             * To disable any of the optional paths, define the environment variable as empty in your default.env file. For
             * example:
             *
             * PRIV_ROOT=""  # this will disable all paths that depend on PRIV_ROOT
             */

            // This should not ever be needed, but we perform a check here to uncover bugs and mistakes.
            'DOCUMENT_ROOT' => function (): ?string {
                if(PHP_SAPI === 'cli') {
                    throw new ConfigException("We can't rely on DOCUMENT_ROOT when not running in a web server. Consider using putenv()");
                }
                if(isset($_SERVER['DOCUMENT_ROOT'])) {
                    return realpath($_SERVER['DOCUMENT_ROOT']);
                }
                throw new MissingConfigException("DOCUMENT_ROOT should already be available when running in the '".PHP_SAPI."' SAPI");
            },

            /**
             * Place to store temporary files. On load balanced web servers and in serverless environments, these files may disappear 
             * and come back between requests.
             *
             * Files may even be deleted during a request.
             */
            'TEMP_ROOT' => function(): ?string {
                $root = sys_get_temp_dir();
                $candidate = $root."/".str_replace(["/", DIRECTORY_SEPARATOR], "_", trim(static::env("ROOT"), DIRECTORY_SEPARATOR."/"));
                $GLOBALS["f2"]["notices"]["F2\\Commmon\\Config::env"]["TEMP_ROOT"] = "Based on sys_get_temp_dir()";
                static::ensureDirExists($candidate);
                return $candidate;
            },

            /**
             * Similar to TEMP_ROOT, but files and directories should NOT be deleted unless the web server is restarted. Scripts should
             * perform their own garbage collection.
             */
            'WORK_ROOT' => function(): ?string {
                $root = static::env('ROOT');
                $root .= '/var';
                if (static::isInsideDocumentRoot($root)) {
                    throw new ConfigException("WORK_ROOT ($root) can't be inside DOCUMENT_ROOT");
                } else {
                    $GLOBALS["f2"]["notices"]["F2\\Commmon\\Config::env"]["WORK_ROOT"] = "Based on ROOT/var";
                }
                static::ensureDirExists($root);
                return $root;
            },

            /**
             * Permanent file storage that MUST be shared between web servers. Can be using NFS or S3FS file systems.
             */
            'SHARED_ROOT' => function(): ?string {
                $root = static::env('ROOT');
                $root .= '/shared';
                if (static::isInsideDocumentRoot($root)) {
                    throw new ConfigException("SHARED_ROOT ($root) can't be inside DOCUMENT_ROOT");
                    // WORK_ROOT is assumed to be outside of DOCUMENT_ROOT
                    $root = static::env('WORK_ROOT').'/shared';
                    $GLOBALS["f2"]["notices"]["F2\\Commmon\\Config::env"]["SHARED_ROOT"] = "Based on WORK_ROOT/shared since ROOT would be inside DOCUMENT_ROOT";
                    static::ensureDirExists($root);
                } else {
                    $GLOBALS["f2"]["notices"]["F2\\Commmon\\Config::env"]["SHARED_ROOT"] = "Based on ROOT/shared";
                }
                throw new MissingConfigException('SHARED_ROOT');
            },

            // Path for permanently storing files. Files are rarely changed, and MUST be accessible via web server. May use S3FS or alternatives.
            // Must match USERFILES_URL
            'USERFILES_ROOT' => function (): ?string {
                if($root = static::env('DOCUMENT_ROOT')) {
                    $root .= '/files';
                    $GLOBALS["f2"]["notices"]["F2\\Commmon\\Config::env"]["USERFILES_ROOT"] = "Based on DOCUMENT_ROOT/files";
                    static::ensureDirExists($root);
                    return $root;
                }
                throw new MissingConfigException('USERFILES_ROOT');
            },

            // Web application source code and files. Ideally placed outside of DOCUMENT_ROOT, but not a requirement.
            'APP_ROOT' => function (): ?string {
                // ROOT always exists
                $root = static::env('ROOT');
                $root .= '/app';
                $GLOBALS["f2"]["notices"]["F2\\Commmon\\Config::env"]["APP_ROOT"] = "Based on ROOT/app";
                if (!is_dir($root)) {
                    throw new ConfigException("APP_ROOT ($root) does not exist");
                }
                return $root;
            },

            // Path to configuration files. Should not be accessible via web server.
            'CONFIG_ROOT' => function (): ?string {
                $root = static::env('APP_ROOT');
                $root .= '/config';
                $GLOBALS["f2"]["notices"]["F2\\Commmon\\Config::env"]["CONFIG_ROOT"] = "Based on APP_ROOT/config";

                // if the directory does not exist, it is probably not needed by the project
        		$root = realpath($root);
                return $root ? $root : null;
            },

            /**
             * Path specific for the project. Should not be confused with files in bundles.
             */
            'VIEWS_ROOT' => function (): ?string {
                $root = static::env('APP_ROOT');
                $root .= '/views';
                $GLOBALS["f2"]["notices"]["F2\\Commmon\\Config::env"]["VIEWS_ROOT"] = "Based on APP_ROOT/views";

                // if the directory does not exist, it is probably not needed by the project
                $root = realpath($root);
                return $root ? $root : null;
            },

            /**
             * Path specific for the project. Should not be confused with files in bundles.
             */
            'SEEDS_ROOT' => function (): ?string {
                $root = static::env('APP_ROOT');
                $root .= '/seeds';
                $GLOBALS["f2"]["notices"]["F2\\Commmon\\Config::env"]["SEEDS_ROOT"] = "Based on APP_ROOT/seeds";

                // if the directory does not exist, it is probably not needed by the project
                $root = realpath($root);
                return $root ? $root : null;
            },

            'MIGRATIONS_ROOT' => function (): ?string {
                $root = static::env('APP_ROOT');
                $root .= '/migrations';
                $GLOBALS["f2"]["notices"]["F2\\Commmon\\Config::env"]["MIGRATIONS_ROOT"] = "Based on APP_ROOT/migrations";

                $root = realpath($root);
                return $root ? $root : null;
            },

            // REQUIRED for web applications
            'BASE_URL' => function (): ?string {
                if (PHP_SAPI === 'cli') {
                    return null;
                }
                $baseUrl = '//' . $_SERVER['HTTP_HOST'];
                $GLOBALS["f2"]["notices"]["F2\\Commmon\\Config::env"]["BASE_URL"] = "Based on HTTP_HOST";
                return $baseUrl;
            },

            // Web path for files in USERFILES_ROOT
            'USERFILES_URL' => function (): ?string {
                if (PHP_SAPI === 'cli') {
                    return null;
                }
                $bu = static::env("BASE_URL");

                // If USERFILES_ROOT is inside DOCUMENT_ROOT we can make some assumptions
                if (($pr = static::env("USERFILES_ROOT")) && static::isInsideDocumentRoot($pr)) {
                    // If USERFILES_ROOT is inside DOCUMENT_ROOT then we can build USERFILES_URL from that with BASE_URL.
                    $dr = static::env("DOCUMENT_ROOT");
                    $pr = realpath($pr);
                    $dr = realpath($dr);
                    if (!$pr || !$dr) {
                        throw new ConfigException("USERFILES_ROOT does not exist");
                    }
                    $drl = strlen($dr);
                    if (substr($pr, 0, $drl) === $dr) {
                        $path = substr($pr, $drl);
                        $GLOBALS["f2"]["notices"]["F2\\Commmon\\Config::env"]["PUBLIC_URL"] = "Based on BASE_URL$path";
                        return $bu.$path;
                    }
                    throw new MissingConfigException('USERFILES_URL');
                } else {
                    $GLOBALS["f2"]["notices"]["F2\\Commmon\\Config::env"]["PUBLIC_URL"] = "Based on BASE_URL/files";
                    return '//' . $_SERVER['HTTP_HOST'] . "/files";
                }
            },

            // Doctrine style database configuration
           'DATABASE_URL' => function (): ?string {

                // Try to detect database url using common environment variables
                // TODO. See docksal.io, OPENSHIFT_MYSQL_DB_HOST, https://docs.craftcms.com/v3/config/db-settings.html

                /**
                 * This fallback is considered OK, since we assume DATABASE_URL will have been configured in a production
                 * environment
                 */
                if ($root = static::env('WORK_ROOT')) {
                    $root .= '/development-database.sqlite';
                    $GLOBALS["f2"]["notices"]["F2\\Commmon\\Config::env"]["DATABASE_URL"] = "Based on WORK_ROOT/development-database.sqlite";
                    return 'sqlite:///' . $root;
                }

                throw new MissingConfigException('DATABASE_URL');
            }
        ];

        if (isset($resolvers[$name])) {
            try {
                return (string) $cached[$name] = call_user_func($resolvers[$name]);
            } catch (\Throwable $e) {
                throw new ConfigException(get_class($e)."(".$e->getMessage()." #".$e->getCode().") getting the '$name' env variable");
            }
        } else {
            return null;
        }
    }

    public static function getComposerJson():array {
        static $cache;
        if ($cache) {
            return $cache;
        }
        return $cache = json_decode(file_get_contents($GLOBALS['f2']['common']['vendor-path'].'/composer.json'), true);
    }

    public static function getComposerClassLoader(): \Composer\Autoload\ClassLoader {
        return require(static::env('ROOT').'/vendor/autoload.php');
    }

    /**
     * Check if the path is inside DOCUMENT_ROOT
     */
    protected static function isInsideDocumentRoot(string $path): bool {
        if(PHP_SAPI === 'cli') {
            // Nothing is inside document root when the SAPI is cli
            return false;
        }
/*
        $rpath = realpath($path);
        if (!$rpath) {
            throw new ConfigException("$path does not exist");
        }
*/

        $parent = str_replace(DIRECTORY_SEPARATOR, '/', rtrim(static::env('DOCUMENT_ROOT'), '/').'/');
        $child = str_replace(DIRECTORY_SEPARATOR, '/', rtrim($path, '/').'/');
        $result = strpos($child, $parent)===0;
        return $result;
    }


    /**
     * Makes sure that the directory exists. If it does not exist, attempt to create it.
     *
     * @todo Should cache this in Optimizer
     */
    protected static function ensureDirExists(string $candidate): void {
        if (!is_dir($candidate)) {
            throw new ConfigException("The path '$candidate' does not exist");

            $m = umask(0);
            if (!mkdir($candidate, 01770)) {
                umask($m);
                throw new ConfigException("Was unable to create directory '$candidate'");
            }
            umask($m);
        }
    }
}
