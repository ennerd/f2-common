<?php
declare(strict_types=1);

namespace F2\Common;

class NotFoundException extends Exception {
    public $httpStatusCode = 404;
    public $httpStatusMessage = 'Not found';
}
