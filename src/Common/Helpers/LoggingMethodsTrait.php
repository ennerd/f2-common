<?php
namespace F2\Common\Helpers;

use Psr\Log\LoggerInterface;
use F2;
use F2\Common\Logging\StderrLogger;

/**
 * This trait is meant to add convenient protected logging methods to any class
 * so that one can do $this->emergency('some-message').
 *
 * Note: We implement the API identical to Psr-3 LoggerInterface, except it is
 * not meant to be used by external classes.
 *
 * The LoggingMethodsTrait::getLogger() method will retrieve the logger instance
 * from the default container.
 *
 */
trait LoggingMethodsTrait {
    protected $logger;

    protected function getLogger(): LoggerInterface {
        if (!$this->logger) {
            $this->logger = F2::container()->get(LoggerInterface::class);
        }
        return $this->logger;
    }

    protected function emergency($message, array $context = []) {
        return $this->getLogger()->emergency($message, $context);
    }

    protected function alert($message, array $context = []) {
        return $this->getLogger()->alert($message, $context);
    }

    protected function critical($message, array $context = []) {
        return $this->getLogger()->critical($message, $context);
    }

    protected function error($message, array $context = []) {
        return $this->getLogger()->error($message, $context);
    }

    protected function warning($message, array $context = []) {
        return $this->getLogger()->warning($message, $context);
    }

    protected function notice($message, array $context = []) {
        return $this->getLogger()->notice($message, $context);
    }

    protected function info($message, array $context = []) {
        return $this->getLogger()->info($message, $context);
    }

    protected function debug($message, array $context = []) {
        return $this->getLogger()->debug($message, $context);
    }
}
