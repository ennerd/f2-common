<?php
namespace F2\Common;

class ConfigException extends Exception {
    public $httpStatusMessage = "Internal configuration error";
}
