<?php
declare(strict_types=1);

namespace F2\Common;

class ConventionException extends LogicException {
    use ExceptionTrait;

    public $httpStatusMessage = "Software error";
}
