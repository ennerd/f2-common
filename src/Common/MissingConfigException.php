<?php
namespace F2\Common;

use F2\Common\Contracts\Exceptions\ExceptionInterface;

class MissingConfigException extends ConfigException {
    public $httpStatusMessage = "Internal configuration missing";

    public function __construct(string $config) {
        parent::__construct("The config named '$config' is missing.");
    }

}
