<?php
declare(strict_types=1);

namespace F2\Common;

abstract class AbstractManifest {

    final public function __construct() {}

    /**
     * If you wish to override default configuration provided by another manifest,
     * you can specify dependencies here. In short; if your package.json depends
     * on another package, you may specify the Manifest class of that package as
     * a depencency. 
     *
     * This ensures that you can override config defaults provided by that other
     * package.
     */
    public function getDependencies(): iterable {
        return [];
    }

    /**
     * If you wish to register methods on the \F2 class, output an array
     * of ['methodName' => 'actual_method']-pairs.
     */
    public function getF2Methods(): iterable {
        return [];
    }

    /**
     * Add config settings to the default config structure. For example
     * ```
     * $config['my-config-key'] = 'some-default';
     * return $config;
     * ```
     *
     * @param array $config Array holding the current config defaults
     * @return array        The updated config defaults
     */
    public function registerConfigDefaults(array $config): array {
        return $config;
    }

}
