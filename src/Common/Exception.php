<?php
declare(strict_types=1);

namespace F2\Common;

use F2\Common\Contracts\ExceptionInterface;

class Exception extends \Exception implements ExceptionInterface {
    use ExceptionTrait;
}
