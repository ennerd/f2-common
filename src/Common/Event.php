<?php
declare(strict_types=1);
namespace F2\Common;

class Event implements Contracts\EventInterface {
    protected $_name;
    protected $_data;
    protected $_isCancelled;

    public function __construct(string $eventName, array $data) {
        if (!isset($data['src'])) {
            throw new LogicException("The event data array must contain 'src' element indicating the source instance or class name of this event.");
        }
        if (!defined($eventName)) {
            throw new ConventionException("Event name '$eventName' should be a constant with a value so that defined(CONSTANT_NAME) is true.");
        }
        $this->_name = $eventName;
        $this->_data = $data;
        $this->_isCancelled = false;
    }

    public function getName(): string {
        return $this->_name;
    }

    public function getData(): array {
        return $this->_data;
    }

    public function isCancelled(): bool {
        return $this->_isCancelled;
    }

    public function cancel(): void {
        $this->_isCancelled = true;
    }

    public function __get(string $name) {
        if ($name === 'name') {
            return $this->getName();
        } elseif ($name === 'cancelled') {
            return $this->isCancelled();
        }
        return $this->_data[$name] ?? null;
    }

    public function __set(string $name, $value) {
        if (!array_key_exists($name, $this->_data)) {
            throw new UndeclaredPropertyException($this, $name);
        }
        $this->_data[$name] = $value;
    }
}
