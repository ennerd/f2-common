<?php
declare(strict_types=1);

namespace F2\Common;

class UndeclaredPropertyException extends LogicException {
    public $httpStatusMessage = "Undeclared property error";

    public function __construct($classOrInstance, string $property) {
        if (is_object($classOrInstance)) {
            $className = get_class($classOrInstance);
        } elseif (class_exists($classOrInstance)) {
            $className = $classOrInstance;
        } else {
            throw new InvalidArgumentException("Expects a class name or an object");
        }
        parent::__construct("Undeclared property $className::$property");
    }
}
