<?php
declare(strict_types=1);

namespace F2\Common;

use Symfony\Component\VarExporter\VarExporter;
use Symfony\Component\VarExporter\Exception\NotInstantiableTypeException;

/**
 * Generates the F2 class according to which F2 components are installed.
 */
class Optimizer {

    protected $root;

    public function __construct(string $root) {
        $this->root = $root;
    }

    public static function getPreloaderPath(): string {
        if (!($root = Config::config('ROOT'))) {
            throw new PreloaderException('Unable to infer the ROOT environment variable');
        }
        if (!function_exists('posix_geteuid')) {
            $uid = file('/proc/self/status');
            foreach ($uid as $line) {
                if (substr($line, 0, 4) === 'Uid:') {
                    $uid = intval(explode("\t", $line)[1]);
                    break;
                }
            }
            if (!is_int($uid)) {
                throw new \Exception("Unable to determine current user id via posix_geteuid or /proc/self/status");
            }
        } else {
            $uid = \posix_geteuid();
        }
        return sys_get_temp_dir().'/F2.'.$uid.'.cache.'.md5($root).'.php';
    }

    public function compilePreloader(string $className = 'F2'): string {
        // Head of the file
        $res = '<?php
declare(strict_types=1);

/**
 * This file is automatically compiled by F2\Common\Optimizer
 * whenever composer.lock is updated.
 */
namespace {

    use F2\Common\Manifest;


    /**
     * GLOBAL NAMESPACE
     */
    class '.$className.' {
';

        $res .= $this->compileConfigDefaults();

        // Discover all functions that need to be compiled into
        // the preloader.
        $res .= $this->compileFunctions();


        $res .= '
        /**
         * Provided as a fallback mechanism, in case new methods become available
         * without the f2-preloader.php being updated.
         */
        public static function __callStatic($name, $arguments) {
            foreach(Manifest::getAllManifests() as $manifest) {
                foreach ($manifest->getF2Methods() as $methodName => $callable) {
                    if ($name === $methodName) {
                        return \call_user_func_array($callable, $arguments);
                    }
                }
            }
            throw new \BadMethodCallException("Method F2::$name() does not exist");
        }
    }

}
';
        return $res;
    }

    protected function compileConfigDefaults():string {
        $defaults = $GLOBALS['f2']['config']['defaults'] ?? [];
        foreach(Manifest::getAllManifests() as $manifest) {
            $defaults = $manifest->registerConfigDefaults($defaults);
        }

        // @TODO: Refactor so that getConfigDefaults() is added from the Manifest instead of hard coded.
        try {
        return
"        /**
         * Returns the config defaults as provided by f2 components
         *
         * @return array Config defaults
         */
        public static function getConfigDefaults(): array {
            return ".VarExporter::export($defaults).";
        }

";
        } catch (NotInstantiableTypeException $e) {
            throw new PreloaderException("Configuration provided via manifests for preloading must be serializable: ".$e->getMessage());
        }
    }

    protected function compileFunctions():string {
        $res = '';
        foreach (Manifest::getAllManifests() as $manifest) {
            foreach ($manifest->getF2Methods() as $functionName => $callable) {
                if (is_string($callable)) {
                    if (is_callable($callable)) {
                        // A normal callable should be added to the class
                        $see = $callable;
                        $ref = new \ReflectionFunction($callable);
                    } elseif (class_exists(\F2\Container\Container::class) && (class_exists($callable) || interface_exists($callable))) {
                        // A normal callable should
                        $see = $callable;
                        $ref = $callable;
                    } else {
                        throw new PreloaderException("Manifest '".get_class($manifest)."' declares a function '$functionName' as '$callable' that can't be implemented.");
                    }
                } elseif (is_array($callable)) {
                    $see = implode("::", $callable);
                    $ref = new \ReflectionMethod($callable[0], $callable[1]);
                } else {
                    throw new PreloaderException("Manifests must provide serializable callbacks via Manifest::getF2Methos()");
                }

                if ($ref instanceof \ReflectionFunctionAbstract) {
                    $params = [];
                    $args = [];
                    $res .= "        /**\n";
                    $res .= "         * @see ".$see."()\n";
                    $res .= "         */\n";
                    $res .= '        public static function ' . $functionName . '(';
                    // Forward a normal function call
                    foreach($ref->getParameters() as $param) {
                        $args[] = ($param->isVariadic() ? "..." : "").'$'.$param->getName();
                        if ($param->hasType()) {
                            $str = $param->getType() . ' '.($param->isVariadic() ? "..." : "").'$' . $param->getName();
                            if ($param->isDefaultValueAvailable()) {
                                $str .= ' = ';
                                if ($param->isDefaultValueConstant()) {
                                    $str .= $param->getDefaultValueConstantName();
                                } else {
                                    $str .= VarExporter::export($param->getDefaultValue());
                                }
                            }
                            $params[] = $str;
                        } else {
                            $str = ($param->isVariadic() ? "..." : "").'$' . $param->getName();
                            if ($param->isDefaultValueAvailable()) {
                                $str .= ' = ';
                                if ($param->isDefaultValueConstant()) {
                                    $str .= $param->getDefaultValueConstantName();
                                } else {
                                    $str .= VarExporter::export($param->getDefaultValue());
                                }
                            }
                            $params[] = $str;
                        }
                    }
                    $res .= implode(", ", $params);
                    $returnType = $ref->getReturnType();
                    if ($returnType) {
                        $returnType = ': ' . ($returnType->allowsNull() ? '?' : '') . $returnType;
                    } else {
                        $returnType = '';
                    }
                    $res .= ')'.$returnType." {\n";
                    $res .= '            '.($ref->getReturnType() && $ref->getReturnType()->__toString() === 'void' ? '' : 'return ');
                    if (is_string($callable)) {
                        $res .= $callable;
                    } else {
                        $res .= $callable[0].'::'.$callable[1];
                    }
                    $res .= '('.implode(", ", $args).");\n";
                    $res .= "        }\n\n";
                } elseif (is_string($ref)) {
                    $res .= "        /**\n";
                    $res .= "         * @see $see\n";
                    $res .= "         */\n";
                    $res .= '        public static function ' . $functionName . '(): \\'.$ref." {\n";
                    $res .= "            return \F2::container()->get(".VarExporter::export($ref).");\n";
//                    $res .= "            return \F2\Container\Container::getInstance()->get(".VarExporter::export($ref).");\n";
                    $res .= "        }\n\n";
                }
            }
        }
        return $res;
    }

}
