<?php
declare(strict_types=1);

namespace F2;

use F2;
use F2\Cmd\Cmd;
use F2\Common\Event;
use F2\Common\LogicException;

class CliHelper extends Cmd {

    const SETUP_EVENT = self::class.'::SETUP_EVENT';

    public $args = [];
    protected $routes = [];

    public function __construct() {
        F2::events()->emit(new Event(self::SETUP_EVENT, ['src' => $this]));
        $this->addDefaultRoutes();
        parent::__construct("f2 framework 2", $this->args, ['command']);

    }

    public function addRoute(string $route, callable $callback, string $information) {
        if (strpos($route, "  ") !== false) {
            throw new LogicException("Don't use double spaces in command routes");
        }
        $parts = explode(" ", $route);
        $root = &$this->routes;
        for ($i = 0; $i < sizeof($parts); $i++) {
            $part = $parts[$i];
            if (!isset($root[$part])) {
                $root[$part] = [];
            }
            $root = &$root[$part];
        }
        $root['_callback'] = $callback;
        $root['_information'] = $information;

        $this->setExtraUsage($this->routes);
    }

    protected function setExtraUsage(array $routes) {
        $extraUsage = "where <command> is one of:\n";
        $line = "    ";
        foreach ($routes as $command => $deeper) {
            $line .= "$command, ";
            if (strlen($line.", $command") > 50) {
                $extraUsage .= "$line\n";
                $line = "    ";
            }
        }
        $extraUsage .= rtrim($line, ", ")."\n";
        $this->extraUsage($extraUsage);
    }

    public function run() {
        $args = $this->argv;
        array_shift($args);
        if (empty($args[0])) {
            $this->usage();
        }

        $root = $this->routes;
        while (sizeof($args) > 0) {
            $arg = array_shift($args);
            if (isset($root[$arg])) {
                $root = $root[$arg];
                $this->commandName .= " ".$arg;
            }
        }

        if (!isset($root['_callback'])) {
            $this->setExtraUsage($root);
            $this->usage();
        } else {
            call_user_func($root['_callback']);
        }
    }

    protected function addDefaultRoutes() {
        $this->addRoute('about', [ $this, 'cliAbout' ], 'Display about information');
        $this->addRoute('help config', [ \F2\Common\Config::class, 'cliHelpConfig' ], 'How to use F2::config()' );
        $this->addRoute('help env', [ \F2\Common\Config::class, 'cliHelpEnv' ], 'How to use F2::env()' );
        $this->addRoute('help container', [ \F2\Container\Container::class, 'cliHelpContainer' ], 'How to use F2::container()');
        $this->addRoute('help container di', [ \F2\Container\Container::class, 'cliHelpContainerDi' ], 'About Dependency Injection and Service Locator');
        $this->addRoute('help events', [ \F2\Common\EventService::class, 'cliHelpEvents' ], 'How to use F2::events()' );
//        $this->addRoute('help cache', [ \F2\Common\Psr\SimpleCache\AbstractSimpleCache::class, 'cliHelpCache' ], 'How to use F2::cache()' );
//        $this->addRoute('help log', [ \F2\Common\Psr\Log\AbstractLogger::class, 'cliHelpLog' ], 'How to use F2::log()');
    }

    protected function cliAbout() {
            echo
"f2 framework
============
f2 framework was developed by Frode Børli as a
minimalistic but powerful framework. Focus is on
creating a framework that needs no training by only
providing a minimal set of tools for developers:

 \\F2::env(\$name)       - Environment vars
 \\F2::config(\$name)    - Configuration data
 \\F2::events()         - An event emitter interface
 \\F2::log()            - PSR-3 Logger Interface
 \\F2::container()      - PSR-11 Container Interface
 \\F2::cache()          - PSR-16 Cache Interface

There is no over-engineered PSR-6, PSR-7, PSR-13,
PSR-14, PSR-15, PSR-17 or PSR-18. Instead, if you need
these, just add your preferred implementation them to
the service container.

Configuration variables have default values, to
minimize the setup labor you need to perform. These
configuration variables are currently defined:
    ROOT, DOCUMENT_ROOT, TEMP_ROOT, WORK_ROOT,
    SHARED_ROOT, USERFILES_ROOT, APP_ROOT,
    CONFIG_ROOT, VIEWS_ROOT, SEEDS_ROOT, MIGRATIONS_ROOT
    BASE_URL, USERFILES_URL, DATABASE_URL.

Many of these configuration variables are rarely used,
but it is nice to have their name defined for when you
need them in the future.

Or you can try the 'f2/swoole' package if you want
to create the next generation of high performance,
API-centric, non-blocking software capable of handling
tens-of-thousands of concurrent connections while still
writing old-school PHP code with \$_GET, \$_POST,
\$_FILES and \$_SERVER at your disposal.

There is no particular concept of 'bundles' like in
some other frameworks - but composer packages can
easily extend the functionality of F2 in a coherent.
way.

Your composer package can transparently integrate with
F2 - without any consequences for other frameworks or
applications without any performance cost, by simply
declaring a Manifest class in your library root
namespace.

";
    }

}
