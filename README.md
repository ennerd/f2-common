f2/common
=========

This library is used by other f2/*-libraries for basic things such as configuration discovery,
common exception classes and performance optimizations.

# F2::events(): F2\Common\Contracts\EventServiceInterface

Provides the central event dispatcher:

## Listening for events

`F2::events()->on('some_event_name', function(F2\Common\Contracts\EventInterface $event) {});`

## Un-listening

`F2::events()->off('some_event_name', $handler);`

## Emitting events

`F2::events()->emit(new F2\Common\Event('some_event_name', ['some' => 'data']));`

# F2::config(string $configName, $defaultValue = null): mixed

Retrieve a configuration value from these sources in order:

1. `$GLOBALS['f2']['config'][$configName]`

2. `F2::config()->env($configName)`

3. `$GLOBALS['f2']['config']['defaults'][$configName]`

4. Config defaults provided by Manifest classes.


# F2::env(string $name): string

Retrieve a configuration value from the environment. Will load a .env file from the project
root. If a value is not found, it will load default.env from project root and check for the
value there.
