<?php
require(__DIR__."/../vendor/autoload.php");

use function F2\asserty;

asserty(F2::config('ROOT') === dirname(__DIR__), 'F2::config("ROOT") seems to return the wrong value.');
