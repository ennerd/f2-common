<?php
declare(strict_types=1);

use F2\Common\Optimizer;

/**
 * Sets up the F2 Optimizer and detects the project root.
 */
(function(): void {
    $previousHandler = set_exception_handler(function(\Throwable $e) use (&$previousHandler){
        if ($previousHandler) {
            // Don't replace an existing error handler. We'll use the existing error handler if it has been configured.
            return $previousHandler($e);
        } else {
            // Prettify the error message
            if (PHP_SAPI !== 'cli') {
                echo "
<div style='margin: 0; padding: 10px 30px; background-color: white; color: black; border: 1px solid #eee;'>
<h1 style='margin:0'>".$e->getMessage()." #".$e->getCode()."</h1>
<p style='margin:0'><em>Unhandled ".get_class($e)." exception</em></p>
<p style='margin:0;padding:0;padding-bottom: 10px'>In file <strong>".$e->getFile()."</strong> on line <strong>".$e->getLine().":</p>
<pre style='margin: 0;padding: 0'>";
echo "<strong style='color:red'>".$e->getMessage()." (code=".$e->getCode().")</strong>\n\n";
                print_r($e->getTrace());
                echo "</pre></div>";
                die();
            } else {
                echo $e->getMessage()." #".$e->getCode()." in file ".$e->getFile()." on line ".$e->getLine()."\n";
                echo "Unhandled '".get_class($e)."' exception\n";
                print_r($e->getTrace());
                die();
            }
        }
    });

    /**
     * Detect project ROOT path - unless it is defined as an environment variable.
     * ROOT is defined as the place where composer.json and the vendor/ directory resides.
     */
    if ($root = getenv('ROOT')) {
        // Since it is set through environment variables, we assume it's not just a default value.
        $GLOBALS['f2']['config']['ROOT'] = $root;
    } else {
        // vendor/autoload.php should have been included, and we can use that to detect the project root.
        foreach(\get_included_files() as $path) {
            if (substr(str_replace(DIRECTORY_SEPARATOR, '/', $path), -20) === '/vendor/autoload.php') {
                // Store it in config default, in case it is overridden somewhere later
                $root = $GLOBALS['f2']['config']['defaults']['ROOT'] = substr($path, 0, -20);
                break;
            }
        }
    }
    if (!$root) {
        throw new Error("F2 must be loaded using composer's 'vendor/autoload.php'.");
    }

    /**
     * Register a custom autoloader, because of the \F2 class, that may or may not
     * exist in temp path.
     */
    spl_autoload_register($f2 = function($className) use(&$f2, $root) {
        /**
         * To reduce number of file accesses, we hide notices - and assume file exists, instead
         * of doing file_exist etc.
         *
         * The error handler will be removed before this function returns.
         */
        set_error_handler(function(){ /* ignore errors */ });

        if ($className === "F2") {
            spl_autoload_unregister($f2);

            $preloaderPath = Optimizer::getPreloaderPath();

            $composerLockTime = filemtime($root.'/composer.lock') ?? filemtime($root.'/vendor/autoload.php') ?? null;
            $preloaderTime = filemtime($preloaderPath) ?? null;

            if ($preloaderTime && $composerLockTime && $preloaderTime > $composerLockTime) {
                try {
                    include( $preloaderPath );
                    if (class_exists(F2::class, false)) {
                        restore_error_handler();
                        return;
                    }
                } catch (Throwable $ignored) {}
            }
            restore_error_handler();

            $time = microtime(true);

            $optimizer = new Optimizer($root);

            try {
                $f2ClassSource = $optimizer->compilePreloader();

        		$tempnam = tempnam(sys_get_temp_dir(), 'F2');
                $res = @file_put_contents($tempnam, $f2ClassSource);
                if ($res) {
                    rename($tempnam, $preloaderPath);
                    require($preloaderPath);
                } else {
                    unlink($tempnam);
                    $file = tmpfile();
                    $path = stream_get_meta_data($file)['uri'];
                    fwrite($file, $f2ClassSource);
                    require($path);
                    unlink($path);
                    trigger_error('Wasted '.(round(10000*(microtime(true)-$time))/10).' ms generating the F2 preloader on the fly.');
                }
            } catch (\Throwable $e) {
                throw $e;
            }
        }
    });
})();
